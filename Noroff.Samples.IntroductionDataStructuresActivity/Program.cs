﻿namespace Noroff.Samples.IntroductionDataStructuresActivity
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // 1. Array Initialization
            int[] numbers = { 1, 2, 3, 4, 5 };
            Console.WriteLine("Initial array: " + String.Join(", ", numbers));

            // 2. Adding an Element to an Array
            int[] largerNumbers = new int[numbers.Length + 1];
            numbers.CopyTo(largerNumbers, 0);
            largerNumbers[largerNumbers.Length - 1] = 6;  // Adding new element
            Console.WriteLine("Array after adding an element: " + String.Join(", ", largerNumbers));

            // 3. Accessing a Specific Element
            Console.WriteLine("Third element in the array: " + numbers[2]);

            // 4. List Demonstration
            List<int> numberList = new List<int> { 1, 2, 3, 4, 5 };
            numberList.Add(6);  // Adding an element
            numberList.Remove(3);  // Removing an element
            Console.WriteLine("List after adding and removing elements: " + String.Join(", ", numberList));

            // 5. HashSet Demonstration
            HashSet<int> numberSet = new HashSet<int> { 1, 2, 2, 3, 4, 5, 5 };
            Console.WriteLine("HashSet (duplicates are automatically excluded): " + String.Join(", ", numberSet));

            // 6. Dictionary Demonstration
            Dictionary<int, string> numberDictionary = new Dictionary<int, string>
            {
                {1, "One"},
                {2, "Two"},
                {3, "Three"}
            };
            numberDictionary.Add(4, "Four");  // Adding a new key-value pair
            Console.WriteLine("Value associated with key 3 in the dictionary: " + numberDictionary[3]);
        }
    }
}