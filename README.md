# C# Data Structures Demonstration

This C# program demonstrates the usage and characteristics of various data structures, including arrays, lists, hash sets, and dictionaries. The code exemplifies how each structure is declared, manipulated, and accessed.

## Features

- **Array Operations:** Demonstrates declaration, initialization, and manual expansion of arrays.
- **List Usage:** Shows the dynamic nature of lists in C#, including adding and removing elements.
- **HashSet Exploration:** Illustrates how hash sets handle duplicates automatically.
- **Dictionary Manipulation:** Demonstrates the creation and usage of dictionaries for storing key-value pairs.

## Running the Program

To run this program, you need to have a C# compiler or an IDE that supports C# development, such as Visual Studio.

1. Run the compiled program.

## How to Use

- The program will display the results of various operations on data structures directly to the console.
- Watch the console output to understand how each data structure behaves with the given operations.

## Contributing

Feel free to fork this repository and enhance the program. Possible improvements include adding more data structures or more complex operations.

## License

This project is released under the MIT License. Feel free to use it as you wish in your own projects.
